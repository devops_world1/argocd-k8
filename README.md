# argocd-k8

This README outlines the process of installing ArgoCD in a Kubernetes cluster using the official ArgoCD manifests. To begin, apply the ArgoCD installation manifests to the 'argocd' namespace using the following command:

`kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml`
After successful installation, access the ArgoCD Web UI by creating a port-forward to the service:

`kubectl port-forward svc/argocd-server 8080:443 -n argocd`

Visit https://localhost:8080 in your browser and log in with the default credentials.
 
With ArgoCD running, you can deploy and synchronize Kubernetes applications by configuring application, specifying the Git repository URL and application path. This guide offers a straightforward setup for ArgoCD installation and continuous deployment using its manifests in a Kubernetes environment. 





